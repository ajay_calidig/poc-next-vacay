install this laravel project using following steps : 

1. clone this repository in your folder path
2. Run command 'composer install' in cli inside the project folder
3. rename or copy the file '.env.example' into '.env'
4. now import the sql file 'nextvacay.sql' in your mysql database. (make sure after imported the db, db name in database should match the db name in your .env file  )
5. after imported the sql file go to 'users' table and edit any user's 'email' column which you want to test.
6. Run command 'php artisan serve' to serve the application and open your browser at http://localhost:8000/


Application flow : 

1. after opening the app in browser a form will be shown at the begining to enter the email address.
2. fill email of already existed user (the user email you've edited in 5th point in above section).
3. when submit the form it will show a message of success and failure message if user not exist or invalid email.
4. check your mail inbox if there is email of name 'Login Request' then click a link inside it to gain access.
5. the link will redirect user to app main page and login them with showing their email id to edit.
6. from here user edit the email and update it.
7. a logout button is provided on top which makes user logout from both server and app section.

we've set the expiry time of access link upto 60 min. if user click the link after 60 min starting from the time when he submit the email, then it will not allow that user to log in and show link expiry message.