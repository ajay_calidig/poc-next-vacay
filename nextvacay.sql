-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2020 at 11:39 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nextvacay`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(9, '2014_10_12_000000_create_users_table', 1),
(10, '2014_10_12_100000_create_password_resets_table', 1),
(11, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(12, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(13, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(14, '2016_06_01_000004_create_oauth_clients_table', 1),
(15, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(16, '2019_08_19_000000_create_failed_jobs_table', 1),
(17, '2020_07_12_075041_create_table_user_token_expire', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('039dbe3c0249f3d78114f16f572b4c6ccf36008f783531fe7c39732ab4b9fb009d2aadb0d77280bf', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 01:27:57', '2020-07-12 01:27:57', '2021-07-12 06:57:57'),
('082e975785ee9b0403f12641670be6beaadf63bbcd2b016475eefaa1559589c77595edb214a04fdf', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 02:59:39', '2020-07-12 02:59:39', '2021-07-12 08:29:39'),
('1164e6a94fe1934ead6a9df00282a700f8458f5ecae82d24ed300b6e9b2acd2d66b92e30706dc19c', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 00:39:29', '2020-07-12 00:39:29', '2021-07-12 06:09:29'),
('12ade1d9799798aed263ae0b6f36f5aca34279f695366bfa0ae0b1ec7fd7e077bde173c5b7bf08d7', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 03:43:40', '2020-07-12 03:43:40', '2021-07-12 09:13:40'),
('21ba71d9f7741332ababbfefc99aac1a9c4746eb6e7cc03010c11c2c375722d6d41824f850878f08', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 01:09:58', '2020-07-12 01:09:58', '2021-07-12 06:39:58'),
('22ae9b17ade81348b08a9232a722bba0bd3beff9098449c229385cb7d9bb40761d77025086f5f1cc', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 01:25:09', '2020-07-12 01:25:09', '2021-07-12 06:55:09'),
('2a165aa6b5d0e899cdaea9c2d7827f19d0ba350d9a3ff6de0c504842b6a26dee184db5de2e3fb410', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 03:48:00', '2020-07-12 03:48:00', '2021-07-12 09:18:00'),
('305b3faf3c1d1d1a271ddb814c23fb8a0a220b708082b7a50d2d8e4539e0e54dbce82ed14b2629a3', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 01:36:44', '2020-07-12 01:36:44', '2021-07-12 07:06:44'),
('306c21b53496ad4a13bd32c69e950e7923df1ae069dab752556d41b62f025ac95dafc29169c12ebd', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 01:10:09', '2020-07-12 01:10:09', '2021-07-12 06:40:09'),
('3b0f07d8f50472e06faff971fa891603bcec0abf6ddbbd51ca0ba89d9a072f60d21d6dbbc91ad9e6', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 01:02:44', '2020-07-12 01:02:44', '2021-07-12 06:32:44'),
('4036005c465d474008adcfd22e94c96dee8a73d7db0251ab4fbed24951849be2120b0704012780b8', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 03:28:00', '2020-07-12 03:28:00', '2021-07-12 08:58:00'),
('4498bb437c313bda50a2afdca67578e6024d899617a92466872269c2b0fcf9c11f51488ddc23ec34', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 00:34:15', '2020-07-12 00:34:15', '2021-07-12 06:04:15'),
('4744c2b599df4e30f0268341d3fc1a0ade62d96636ef53773184c238d8499f352ab8206d0eb07daa', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 03:11:53', '2020-07-12 03:11:53', '2021-07-12 08:41:53'),
('4e54fd732a5b06371589551ac438c51ca9b02094f3a428ad7240d292d6065fc9b42d28ce03d3ff96', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 01:10:11', '2020-07-12 01:10:11', '2021-07-12 06:40:11'),
('564d4536a8d8af49218b7544092b006947fc1b4325b3426a0f5eab6f49637dd25388f6da977cc570', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 00:24:10', '2020-07-12 00:24:10', '2021-07-12 05:54:10'),
('6e05a2ba933e1c6a69264ced281336d5d0c6e96630975002977f6589a151c476fccdf4b5aa827eae', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 00:37:06', '2020-07-12 00:37:06', '2021-07-12 06:07:06'),
('6eaad482bb0826d002e34ecc505e4fc5ae06cf6e0fa0fb8b96920f61f4dbe7abfcc72208f98a9900', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 01:02:16', '2020-07-12 01:02:16', '2021-07-12 06:32:16'),
('737afaea9bd9b4d9bd27e39f6c72962fdc63d75c11cb18559839d11d122d1107984017628b2c4b54', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 02:58:52', '2020-07-12 02:58:52', '2021-07-12 08:28:52'),
('77a1e49c0533ae8d248722cc79231c05abfeb9d74efa4b1c757e4775e59ed6c97bc94ca8c27c9aa2', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 00:34:36', '2020-07-12 00:34:36', '2021-07-12 06:04:36'),
('7b25d6f984065406095de3686defaf04866a02b286d03e2cd1d6f42685f67c2d32afb16ae0470ee2', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 03:04:29', '2020-07-12 03:04:29', '2021-07-12 08:34:29'),
('7e0d74228ffd51faee1ec23d93fa7527a6d29b95ac4a53259586dbd470beeae78a88275ea5148247', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-11 05:40:52', '2020-07-11 05:40:52', '2021-07-11 11:10:52'),
('8490cb6b6a24c036b264945bc41415295405782b380c22784453552c8b875fb8e878a815b9c2c32b', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 00:38:17', '2020-07-12 00:38:17', '2021-07-12 06:08:17'),
('86b1b6d922577966eaacdc7c4ec328ed22fe3b907d4b3e954417ab2324ac961062843d816bba6b83', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 00:24:51', '2020-07-12 00:24:51', '2021-07-12 05:54:51'),
('8ab52daf4edb595e4374ac73b5c4e7c2bf3336a343b0eb5b51c5491724a37cdfd7595ca117cbbbae', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 01:31:59', '2020-07-12 01:31:59', '2021-07-12 07:01:59'),
('a1b3af140c77013a335823745ec3294214c0b1c979689c75088610761a6680ca9301987421725156', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 03:17:36', '2020-07-12 03:17:36', '2021-07-12 08:47:36'),
('bab3b9c78d0ab8aaa6ca5f21d4295adf79b0639325bd618f0845f8000a85c465e2625e9ceb7e2270', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 00:30:37', '2020-07-12 00:30:37', '2021-07-12 06:00:37'),
('bfd0609896bc52d38a49455802c40051a6127608ad833839d17b7f3b70b83fe719f77d283d0b21a4', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 01:07:06', '2020-07-12 01:07:06', '2021-07-12 06:37:06'),
('c23fc8c5ebdb178cb512f0a89e374c015ffecd43260e50f6cf981008137e54de2cd4017500569d77', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-11 05:49:00', '2020-07-11 05:49:00', '2021-07-11 11:19:00'),
('c910ac29576af8019af78224ad5f3d1624f2e8cdebeb3281ecbe2edf0cdf0d89ad3b26c3b322fb6f', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 00:35:08', '2020-07-12 00:35:08', '2021-07-12 06:05:08'),
('c9ff1687690ed094fb4a8ade5fca715912dbb4fb4f2f271b6fad9938ac3524e9b5f2d58394c99c18', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 01:31:17', '2020-07-12 01:31:17', '2021-07-12 07:01:17'),
('d4dc8089be71a88ce6b43f59d451c87dd54a3a0f0a1646c80d68957c8108d4ff4ed671157c849593', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 00:39:40', '2020-07-12 00:39:40', '2021-07-12 06:09:40'),
('e30685019b5c32f63dd4f324a36e7cb24836995c833fa9aa05f443136b3624d40f9b09938542678d', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 00:43:04', '2020-07-12 00:43:04', '2021-07-12 06:13:04'),
('e89abbb329c8c4090d7f3e7174be6750bf1d45145e16620951fb4ba09219e9790386662aff5be75e', 1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', 'Token Name', '[]', 1, '2020-07-12 02:54:03', '2020-07-12 02:54:03', '2021-07-12 08:24:03');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
('9103d612-8ed9-4ac6-8c27-870529c5f21f', NULL, 'Laravel Personal Access Client', 'kZyFOS6LMTHr3Vs2kTtYuKa1Zvenq2aUCuTf4W9L', NULL, 'http://localhost', 1, 0, 0, '2020-07-11 03:07:04', '2020-07-11 03:07:04'),
('9103d613-014c-4517-8d83-4d6a92e3eb45', NULL, 'Laravel Password Grant Client', 'sKW7MypWd72kM5VKntJmsOfRzqcmzFeWGDRV2ZAG', 'users', 'http://localhost', 0, 1, 0, '2020-07-11 03:07:04', '2020-07-11 03:07:04');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, '9103d612-8ed9-4ac6-8c27-870529c5f21f', '2020-07-11 03:07:04', '2020-07-11 03:07:04');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'user1', 'ajay.singh@calidig.com', NULL, '$2y$10$iuLmbyEkYMZ8b3zT/ATlYOIS3AUqVUje5Za3GlRUXTOhe.yYMcmiW', NULL, '2020-07-11 03:14:29', '2020-07-12 02:13:01'),
(2, 'user2', 'user2@app.com', NULL, '$2y$10$q2iAvNLYWFrkLTWR8TUExuXrhM5aCef08DOVzcyqJmnYh5Y55Ri/6', NULL, '2020-07-11 03:14:29', '2020-07-11 03:14:29'),
(3, 'user3', 'user3@app.com', NULL, '$2y$10$YrBU80SJ8Yor..KPLXUjxu6BsMZdHHVEVMGm4936BInx0o8PHIp6a', NULL, '2020-07-11 03:14:30', '2020-07-11 03:14:30'),
(4, 'user4', 'user4@app.com', NULL, '$2y$10$Icq52BoB7EpoVS1aIzolFOMhHCxPQyd5GCKx2B2IUS.DetXACtKmi', NULL, '2020-07-11 03:14:30', '2020-07-11 03:14:30');

-- --------------------------------------------------------

--
-- Table structure for table `user_verified_token`
--

CREATE TABLE `user_verified_token` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `activation_token` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_verified_token`
--
ALTER TABLE `user_verified_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_verified_token`
--
ALTER TABLE `user_verified_token`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
