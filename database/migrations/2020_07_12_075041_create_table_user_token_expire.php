<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUserTokenExpire extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('user_verified_token', function (Blueprint $table) {
            $table->increments('id');
		   $table->integer('user_id')->unsigned();
		   $table->longText('activation_token');
		   $table->boolean('status')->default(false);
		   $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_verified_token');
    }
}
