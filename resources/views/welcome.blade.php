<!doctype html>

<style>
.hide{display:none}
</style>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <!-- Boostrap 4 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.4/css/bootstrap.css">

    </head>
    <body>
        <div class="container" style="margin: 0px 300px">
        <div class="card">
                <div class="card-body login">  
        <form id="myForm"  style="margin: 40px 20px">
					<label class="alert alert-info email-label hide col-md-12">Sending Email ...</label>
                    <div class="form-group">
                    <label >Email address</label>
                    <input type="email" name="email" class="form-control" placeholder="Enter email">
                    
                    </div>

                    <button type="submit" id="loginBtn" class="btn btn-primary">Submit</button>
               
        </form>
        </div>
		<div class="card-body dash hide">  
        <form id="myForm1"  style="margin: 40px 20px">
					<div class=""><button type="button" class="form-control logout">Logout</button></div>
                    <div class="form-group">
                    <label >Edit Email address</label>
                    <input type="email" name="email" class="form-control" placeholder="Enter email">
                    
                    </div>

                    <button type="submit" id="loginBtn" class="btn btn-primary">Submit</button>
               
        </form>
        </div>
            </div>
        </div>
        <!-- JQuery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>
            $(function() {
				if(localStorage.getItem("access_token")){
					$.ajax({
					  type: "GET",
					  beforeSend: function (xhr) {
							xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem("access_token"));
						},
					  url: '{{url("api/user")}}',
					  dataType: 'json',
					  success: function (result){
						  console.log(result);
						  $(".card-body.dash input[name='email']").val(result.email);
						$(".card-body.login").addClass("hide");
						$(".card-body.dash").removeClass("hide");
						// window.location.href="/dash";
					  },
					  error(e,data){
						   localStorage. removeItem('access_token');
						 alert(e.responseJSON.message) 
					  }
					});
				}else{
					var url = window.location.href;
				console.log(url.split("_access="));
				if(url.split("_access=")[1] != undefined){
					$.ajax({
					  type: "GET",
					  data: {'_access':url.split("_access=")[1]},
					  url: '{{url("api/gettoken")}}',
					  dataType: 'json',
					  success: function (result){
						  console.log(result);
						  localStorage.setItem("access_token",result.data);
						  window.location.href="/";
						  /*$.ajax({
							  type: "GET",
							  beforeSend: function (xhr) {
									xhr.setRequestHeader('Authorization', 'Bearer '+result.data);
								},
							  url: '{{url("api/user")}}',
							  dataType: 'json',
							  success: function (result){
								  console.log(result);
								  $(".card-body.dash input[name='email']").val(result.email);
								$(".card-body.login").addClass("hide");
								$(".card-body.dash").removeClass("hide");
								// window.location.href="/dash";
							  },
							  error(e,data){
								 alert(e.responseJSON.message) 
							  }
							});*/
					  },
					  error(e,data){
						 alert(e.responseJSON.message) 
					  }
					});
				}
				}
				
				$(".logout").click(function(e){
					e.preventDefault();
					$.ajax({
					  type: "GET",
					  beforeSend: function (xhr) {
							xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem("access_token"));
						},
					  url: '{{url("api/token/revoke")}}',
					  dataType: 'json',
					  success: function (result){
						  console.log(result);
						 localStorage. removeItem('access_token');
						$(".card-body.login").removeClass("hide");
						$(".card-body.dash").addClass("hide");
						// window.location.href="/dash";
					  },
					  error(e,data){
						 alert(e.responseJSON.message) 
					  }
					});
				})
				
                // loginBtn clicked
                $('#myForm').submit( function(e) {
					e.preventDefault();  
					$(".email-label").removeClass("hide");
                    // codes when btn is clicked
                    var formData = $('#myForm').serializeArray();
                    // POST Our Data to laravel Controller
                    $.ajax({
					  type: "POST",
					  data:{email:$('#myForm input[name="email"]').val()},
					  url: '{{url("api/processMyForm")}}',
					  dataType: 'json',
					  success: function (result){
						  $(".email-label").addClass("hide");
						  alert(result.message);
					  },
					  error(e,data){
						  $(".email-label").addClass("hide");
						 alert(e.responseJSON.message) 
					  }
					});
                });
				
				// loginBtn clicked
                $('#myForm1').submit( function(e) {
					e.preventDefault();  
                    // codes when btn is clicked
                    var formData = $('#myForm').serializeArray();
                    // POST Our Data to laravel Controller
                    $.ajax({
					  type: "POST",
					  beforeSend: function (xhr) {
							xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem("access_token"));
						},
					  data:{email:$('#myForm1 input[name="email"]').val()},
					  url: '{{url("api/edit")}}',
					  dataType: 'json',
					  success: function (result){
						  alert(result.message);
					  },
					  error(e,data){
						 alert(e.responseJSON.message) 
					  }
					});
                }); 
				});
        </script>
    </body>
</html>