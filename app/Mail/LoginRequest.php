<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\UserVerifiedToken;
use Illuminate\Support\Facades\Crypt;


class LoginRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
	 public $user;
     public $url;
	 
    public function __construct(User $user)
    {
       $this->user = $user;
	   $encrypted = Crypt::encryptString($user->email);
	   $encrypted = Crypt::encryptString($user->email);
	  
	 $UserVerifiedToken = UserVerifiedToken::create(['user_id'=>$user->id,'activation_token'=>$encrypted])->first();
	   $this->url = config('app.url').'/?_access='.$encrypted;
		  
		  /*if($user->is_verified == false){
			  $encrypted = Crypt::encryptString($user->email);
			  
			  $UserVerifiedToken = UserVerifiedToken::where('user_id',$user->id)->first();
			  if(!empty($UserVerifiedToken)){
				$UserVerifiedToken->delete();
			  }
			  
			 $UserVerifiedToken = UserVerifiedToken::create(['user_id'=>$user->id,'activation_token'=>$encrypted])->first();

			$this->url = config('app.url').'/login?_activate='.$encrypted;
		  }else{
			$this->url = config('app.url').'/login';
		  }*/
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		return $this->from(['address' => 'no-reply@caliapp.com', 'name' => 'caliapp'])
               ->markdown('email.loginrequest');
    }
}
