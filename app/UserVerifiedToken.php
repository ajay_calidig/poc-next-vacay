<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserVerifiedToken extends Model
{
	protected $table='user_verified_token';
	
     protected $fillable = [
          'user_id', 'activation_token', 'status'
     ];
}
