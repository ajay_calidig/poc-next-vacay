<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\UserVerifiedToken;
use Carbon\Carbon;
use App\Mail\LoginRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;

class TestController extends Controller
{
    public function index(Request $request)
    {
		try{
			$user = User::where('email',$request->email)->first();
			if(!$user){
				return response()->json([
					'success' => false,
					'title' => 'failure',
					'message' => 'user not existed !',
					'data' => null
				], 500);
				
			}
			$user = User::find($user->id);
			Mail::to($request->email)->send(new LoginRequest($user));
		 return response()->json([
				'success' => true,
				'title' => 'Success',
				'message' => 'email sent successfully !',
				'data' => null
			], 200);
		 }catch(\Exception $e){
               return response()->json([
                    'type' => 'error',
                    'title' => 'Exception Oops..',
                    'html' => $e->getMessage(),
                    'data' => null
               ], 500);
          }
    }
	
	
    public function login(Request $request)
    {
		try{
			$encode = $request->input('_access');
			if($encode != ""){
				
				try {
					$encrypted = Crypt::decryptString($encode);
				} catch(\RuntimeException $e) {
					$message = "Activation link is not valid.";
					 return response()->json([
						'success' => false,
						'title' => 'failure',
						'message' => $message,
						'data' => $request->all()
					], 500);
				}
			
				$user = User::where('email',$encrypted)->first();
				if(!empty($user)){
					$UserVerifiedToken = UserVerifiedToken::where(['user_id'=>$user->id,'activation_token'=>$encode])->first();
			  if(!empty($UserVerifiedToken)){
				
				$date1 = date('Y-m-d H:i:s');
				$date2 = $UserVerifiedToken->created_at;
				
				$to = Carbon::createFromFormat('Y-m-d H:i:s', $date2);

				$from = Carbon::createFromFormat('Y-m-d H:i:s', $date1);

				$diff_in_hours = $to->diffInMinutes($from);
				
				if($diff_in_hours <= 60 && $UserVerifiedToken->status == 0){
					$UserVerifiedToken->status = 1;
					$UserVerifiedToken->save();
					$token = $user->createToken('Token Name')->accessToken;
					 return response()->json([
							'success' => true,
							'title' => 'Success',
							'message' => 'token recieved successfully !',
							'data' => $token
						], 200);
					}else{
						return response()->json([
							'success' => false,
							'title' => 'failure',
							'message' => 'link is expired !',
							'data' => $request->all()
						], 500);
					}
				  }else{
					  return response()->json([
							'success' => false,
							'title' => 'failure',
							'message' => 'link is expired or not found!',
							'data' => $request->all()
						], 500);
				  }
					
				}else{
					return response()->json([
						'success' => false,
						'title' => 'failure',
						'message' => 'user not found !',
						'data' => $request->all()
					], 500);
				}
			}
			// Creating a token without scopes...
			
		 }catch(\Exception $e){
               return response()->json([
                    'type' => 'error',
                    'title' => 'Exception Oops..',
                    'html' => $e->getMessage(),
                    'data' => null
               ], 500);
          }
    }
	
	public function edit(Request $request)
	{
				try{
				$userdetail = $request->user();
			$emailcount = User::where('email',$request->email)->get();
			$check = $request->user()->email == $request->email ? 1 : 0;
			if($emailcount->count() > 0 && $check == 0){
				return response()->json([
					'success' => false,
					'title' => 'failure',
					'message' => 'email already exists!',
					'data' => null
				], 500);
			}
			$userdetail->email = $request->email;
			$userdetail->save();
			
		 return response()->json([
				'success' => true,
				'title' => 'Success',
				'message' => 'account updated !',
				'data' => $userdetail
			], 200);
		 }catch(\Exception $e){
               return response()->json([
                    'type' => 'error',
                    'title' => 'Exception Oops..',
                    'html' => $e->getMessage(),
                    'data' => null
               ], 500);
          }
	}

}
