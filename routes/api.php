<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\UserVerifiedToken;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/processMyForm','TestController@index');

Route::get('/gettoken','TestController@login');
Route::group(['middleware' => ['auth:api']], function(){
	
	
	
	Route::get('/token/revoke', function (Request $request) {
		DB::table('oauth_access_tokens')
			->where('user_id', $request->user()->id)
			->update([
				'revoked' => true
			]);
		$UserVerifiedToken = UserVerifiedToken::where(['user_id'=>$request->user()->id])->get();
		if($UserVerifiedToken->count() > 0){
			UserVerifiedToken::where(['user_id'=>$request->user()->id])->delete();
		}
		return response()->json('DONE');
	});
	
	Route::post('/edit','TestController@edit');
});


